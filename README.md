# README #

on july 12 2016 I made a copy of the CB-API autotests to put in this repository.
This is to be used for training

### What is this repository for? ###

* readyAPI autotests
* dated to 7/12/2016
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* clone the repo from the above 3-dot (...) icon, follow tutorials to practice, commits, merges, branching, features, release, ci process etc.
* Dependencies - one should use this code with ready!API. However, feel free to dump any other code into this repository for training purposes

### Who do I talk to? ###

* Smadar
* Martin/Rick can help when I am not around