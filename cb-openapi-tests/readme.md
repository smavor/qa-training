#Introduction#
This project contains all integration tests for OpenAPI. To date, these tests are SoapUI automated tests that can be ran via the ReadyAPI tool. The project consists of two Maven modules which can be setup to run via continuous integration. The CI setup is not currently complete (update this readme when that happens)

#Prerequistes#
* maven 3.x - if using continuous integration to run the tests
* Ready API - if using SoapUI to run the tests

#RA Setup#
Tests are located in ``IntegrationTests/src/test/resources``. Within this directory are folders representing the API version. Within each version are folders that represent a test project.
<pre>
src/test/resources
   3.2/
       api-agent-write
       ...
   ...
</pre>

Simply navigate to a project folder using RA to open it.
